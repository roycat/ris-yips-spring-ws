package com.roycat.yips.spring.ws.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.roycat.yips.spring.ws.api.UserDao;
import com.roycat.yips.spring.ws.models.User;

@Component
public class MysqlJdbcUserDao extends AbstractJdbcDao implements UserDao {
        
    private DataSource dataSource;
    
    @Autowired
    public MysqlJdbcUserDao(final DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<User> retrieveUsers() throws SQLException {
        List<User> result = new ArrayList<>();
        
        String sql = "select id, email, username from app_user";

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = this.dataSource.getConnection();

            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                result.add(this.populateUserModel(rs));
            }

        } catch (SQLException e) {
            throw new SQLException("Failed to retrieve all user records.", e);
        } finally {
            super.close(conn, ps, rs);
        }
        return result;
    }

    @Override
    public User retrieveUserById(Long id) throws SQLException {
        
        User result = null;
        String sql = "select id, email, username from app_user where id=?";

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = this.dataSource.getConnection();

            ps = conn.prepareStatement(sql);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                result = this.populateUserModel(rs);
            }

        } catch (SQLException e) {
            throw new SQLException("Failed to retrieve user record where id = " + id + ".", e);
        } finally {
            super.close(conn, ps, rs);
        }
        return result;
    }

    @Override
    public User retrieveUserByUsername(String username) throws SQLException {
        
        User result = null;
        String sql = "select id, email, username, pword as password from app_user where username=?";
    
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
    
        try {
            conn = this.dataSource.getConnection();
    
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            rs = ps.executeQuery();
            while (rs.next()) {
                result = this.populateUserModel(rs);
                result.setPassword(rs.getString("password"));
            }
    
        } catch (SQLException e) {
            throw new SQLException("Failed to retrieve user record where username = " + username + ".", e);
        } finally {
            super.close(conn, ps, rs);
        }
        return result;
    }

    @Override
    public List<User> searchUsersByUsername(String username) throws SQLException {
        
        List<User> result = null;
        String sql = "select id, email, username from app_user where userName like ?";
    
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
    
        try {
            result = new ArrayList<>();
            conn = this.dataSource.getConnection();
    
            ps = conn.prepareStatement(sql);
            ps.setString(1, "%" + username + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                result.add(this.populateUserModel(rs));
            }
    
        } catch (SQLException e) {
            throw new SQLException("Failed to retrieve user record where username like %" + username + "%.", e);
        } finally {
            super.close(conn, ps, rs);
        }
        return result;
    }

    @Override
    public List<User> searchUsersByEmail(String email) throws SQLException {
        
        List<User> result = null;
        String sql = "select id, email, username from app_user where email like ?";
    
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
    
        try {
            result = new ArrayList<>();
            conn = this.dataSource.getConnection();
    
            ps = conn.prepareStatement(sql);
            ps.setString(1, "%" + email + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                result.add(this.populateUserModel(rs));
            }
    
        } catch (SQLException e) {
            throw new SQLException("Failed to retrieve user record where email like %" + email + "%.", e);
        } finally {
            super.close(conn, ps, rs);
        }
        return result;
    }

    @Override
    public void createUser(User user) throws SQLException {
        String sql = "insert into app_user (pword, email, username) values (?, ?, ?)";
    
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
    
        try {
            conn = this.dataSource.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, user.getPassword());
            ps.setString(2, user.getEmail());
            ps.setString(3, user.getUsername());
            ps.execute();
    
        } catch (SQLException e) {
            throw new SQLException("Failed to create user where user = " + user + ".", e);
        } finally {
            super.close(conn, ps, rs);
        }        
    }

    private User populateUserModel(ResultSet rs) throws SQLException {
        User result = new User();
        result.setEmail(rs.getString("email"));
        result.setId(rs.getLong("id"));
        result.setUsername(rs.getString("username"));
        return result;
    }

}
