package com.roycat.yips.spring.ws.models;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.roycat.yips.spring.ws.spring.CustomLocalDateTimeDeserializer;
import com.roycat.yips.spring.ws.spring.CustomLocalDateTimeSerializer;

public class Message implements Serializable {
    
    private static final long serialVersionUID = 6458280993995872951L;
    
    private Long id;
    private Long fromUserId;
    private Long toUserId;
    private String text;
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    private LocalDateTime createdTime;

    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }
    public LocalDateTime getCreatedTime() {
        return createdTime;
    }
    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }
    public Long getFromUserId() {
        return fromUserId;
    }
    public void setFromUserId(Long fromUserId) {
        this.fromUserId = fromUserId;
    }
    public Long getToUserId() {
        return toUserId;
    }
    public void setToUserId(Long toUserId) {
        this.toUserId = toUserId;
    }
    @Override
    public String toString() {
        return "Message [id=" + id + ", fromUserId=" + fromUserId + ", toUserId=" + toUserId + ", text=" + text
            + ", createdTime=" + createdTime + "]";
    }

}
