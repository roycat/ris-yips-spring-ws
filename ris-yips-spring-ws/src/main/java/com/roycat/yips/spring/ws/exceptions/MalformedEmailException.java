package com.roycat.yips.spring.ws.exceptions;

public class MalformedEmailException extends Exception {

    private static final long serialVersionUID = 7603130804164894065L;

    public MalformedEmailException() {
        super();
    }

    public MalformedEmailException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public MalformedEmailException(final String message) {
        super(message);
    }

    public MalformedEmailException(final Throwable cause) {
        super(cause);
    }

}
