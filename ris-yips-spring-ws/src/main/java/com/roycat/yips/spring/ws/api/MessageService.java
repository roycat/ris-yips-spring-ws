package com.roycat.yips.spring.ws.api;

import java.sql.SQLException;
import java.util.List;

import com.roycat.yips.spring.ws.models.Message;

/**
 * This interface models all services related to Message objects.
 * @author Steve Bolin - Apr 2018
 *
 */
public interface MessageService {
    
    /**
     * Creates a record in the database for the provided Message object.
     * The id in the user object is ignored as the method is responsible 
     * for id generation.
     * @param message The message to be created.
     * @throws SQLException
     */
    void createMessage(Message message) throws SQLException;
    
    /**
     * Retrieves a List of all Messages between userId1 and userId2. If no messages 
     * found, returns an empty List.
     * @param userId1 The user id of one of the users 
     * @param userId2 The user id of the other user
     * @return a List of all Messages between userId1 and userId2.
     * @throws SQLException
     */
    List<Message> retrieveMessagesBetween(Long userId1, Long userId2) throws SQLException;
    
}
