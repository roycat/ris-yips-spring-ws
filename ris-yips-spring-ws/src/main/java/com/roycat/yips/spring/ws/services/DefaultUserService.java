package com.roycat.yips.spring.ws.services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.roycat.yips.spring.ws.api.UserDao;
import com.roycat.yips.spring.ws.api.UserService;
import com.roycat.yips.spring.ws.exceptions.DuplicateEmailException;
import com.roycat.yips.spring.ws.exceptions.DuplicateUsernameException;
import com.roycat.yips.spring.ws.exceptions.InvalidPasswordException;
import com.roycat.yips.spring.ws.exceptions.MalformedEmailException;
import com.roycat.yips.spring.ws.models.User;

/**
 * This class implements all services related to User objects.
 * @author Steve Bolin - Apr 2018
 */
@Component
public class DefaultUserService implements UserService {

    private static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
        Pattern.CASE_INSENSITIVE);
    private static final Pattern VALID_PASSWORD_REGEX = Pattern.compile("^[a-zA-Z0-9]+$");
    private static final String PW_MASK = "***";

    private UserDao userDao;

    @Autowired
    public DefaultUserService(final UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public List<String> retrieveUsernames() throws SQLException {
        List<User> users = this.userDao.retrieveUsers();
        List<String> result = new ArrayList<>(users.size());

        for (User user : users) {
            result.add(user.getUsername());
        }
        return result;
    }

    @Override
    public List<User> retrieveUsers() throws SQLException {
        List<User> result = this.userDao.retrieveUsers();

        for (User user : result) {
            this.maskPassword(user);
        }
        return result;
    }

    @Override
    public List<User> searchUsersByUsername(final String username) throws SQLException {
        List<User> result = this.userDao.searchUsersByUsername(username);
        for (User user : result) {
            this.maskPassword(user);
        }
        return result;
    }

    @Override
    public User retrieveUserById(final Long id) throws SQLException {
        User result = this.userDao.retrieveUserById(id);
        maskPassword(result);
        return result;
    }

    @Override
    public User retrieveUserByUsername(String username) throws SQLException {
        
        return this.userDao.retrieveUserByUsername(username);
    }

    @Override
    public List<User> searchUsersByEmail(final String email) throws SQLException {
        List<User> result = this.userDao.searchUsersByEmail(email);
        for (User user : result) {
            this.maskPassword(user);
        }
        return result;
    }

    @Override
    public void registerUser(final User user) throws SQLException, DuplicateEmailException, MalformedEmailException, InvalidPasswordException, DuplicateUsernameException {

        // usernames are reqd to be unique.
        if (!this.searchUsersByUsername(user.getUsername()).isEmpty()) {
            throw new DuplicateUsernameException("A user with this username already exists.");
        }
        
        // Check email string
        if (!this.isValidEmail(user.getEmail())) {
            throw new MalformedEmailException("The email address is not a properly formed email address.");
        }
        
        if (!this.isValidPassword(user.getPassword())) {
            throw new InvalidPasswordException("The password is not a valid password configuration.");
        }

        // emails are reqd to be unique.
        if (!this.searchUsersByEmail(user.getEmail()).isEmpty()) {
            throw new DuplicateEmailException("A user with this email already exists.");
        }

        // Don't accept supplied ids for new users. The id is created by the db in this demo.
        if (user.getId() != null) {
            user.setId(null);
        }
        
        // If it survives the checks, then create the user.
        this.userDao.createUser(user);

    }

    private boolean isValidEmail(String email) {
        if (email == null || email.length() == 0) {
            return false;
        }
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
        return matcher.find();
    }

    private boolean isValidPassword(String password) {
        if (password == null || password.length() == 0) {
            return false;
        }
        Matcher matcher = VALID_PASSWORD_REGEX.matcher(password);
        return matcher.find();
    }

    private void maskPassword(User user) {
        if (null != user) {
            user.setPassword(PW_MASK);
        }
    }

}
