package com.roycat.yips.spring.ws.api;

import javax.xml.ws.WebServiceException;

import org.springframework.http.ResponseEntity;

/**
 * This interface models the controller related to registration.<br />
 * RequestMapping("/register")
 * @author Steve Bolin - Apr 2018
 */
public interface RegistrationController {

    /**
     * Registers a user with the system. <br />
     * Registration requirements:
     * username and email must be unique, the email must be a properly
     * formatted email address, and the password must be at least one
     * alphanumeric character.
     * <br />
     * <b>JSON Format of User object to submit.</b><br />
     * <code>
     * { <br />
     *  "username": "Yips1",<br />
     *  "email": "test1@yiptest.com",<br />
     *  "password": "putYourPasswordHere"<br />
     *  }
     * </code>
     * <br />
     * RequestMapping(value = "/user", method = RequestMethod.POST)
     * @param json content of the request body.
     * @return A response  indicating ACCEPTED, or UNPROCESSABLE_ENTITY. If UNPROCESSABLE_ENTITY, the Exception name will 
     * be returned as content.
     * @throws WebServiceException
     */
    ResponseEntity<?> register(String json) throws WebServiceException;

}
