package com.roycat.yips.spring.ws.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.roycat.yips.spring.ws.constants.RoleConstants;

public class BasicUserDetails implements UserDetails {

    private static final long serialVersionUID = -7418002566794884214L;

    private String username;
    private String password;
    
    public BasicUserDetails() {
        
    }
    
    public BasicUserDetails(final User user) {
        this.username = user.getUsername();
        this.password = user.getPassword();
    }
    
    public BasicUserDetails(final String username, final String password) {
        this.username = username;
        this.password = password;
    }
    
    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    // For the sake of this demo, the following methods are set
    // to fixed values
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> result = new ArrayList<>();
        result.add(new SimpleGrantedAuthority(RoleConstants.USER));
        return result;
    }
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    @Override
    public boolean isEnabled() {
        return true;
    }
}
