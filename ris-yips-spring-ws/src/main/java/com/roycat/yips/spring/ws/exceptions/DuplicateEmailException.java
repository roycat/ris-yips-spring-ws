package com.roycat.yips.spring.ws.exceptions;

public class DuplicateEmailException extends Exception {

    private static final long serialVersionUID = 7603130804164894062L;

    public DuplicateEmailException() {
        super();
    }

    public DuplicateEmailException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public DuplicateEmailException(final String message) {
        super(message);
    }

    public DuplicateEmailException(final Throwable cause) {
        super(cause);
    }

}
