package com.roycat.yips.spring.ws.constants;

/**
 * Constants for different user roles.
 * @author Steve Bolin - April 2018
 */
public final class RoleConstants {
    
    // Utility class. No instantiation.
    private RoleConstants() {}
    
    public static String USER = "ROLE_USER";
    
    // Additional roles added here if needed.

}
