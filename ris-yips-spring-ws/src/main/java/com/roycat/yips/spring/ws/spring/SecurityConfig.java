package com.roycat.yips.spring.ws.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = {
                              "com.roycat.yips.spring.ws.controllers", 
                              "com.roycat.yips.spring.ws.daos", 
                              "com.roycat.yips.spring.ws.services", 
                              "com.roycat.yips.spring.ws.spring"
                             })
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    
    @Autowired
    private CustomAuthenticationProvider authProvider;
    
    @Override
    protected void configure(
      AuthenticationManagerBuilder auth) throws Exception {
  
        auth.authenticationProvider(authProvider);
    }
    
    // Disabling Cross Site Request Forgery (CSRF) for this demo
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
        .csrf().disable()
        .antMatcher("/chat/**")
        .authorizeRequests()
            .anyRequest().authenticated()
            .and()
        .httpBasic();
    }

}
