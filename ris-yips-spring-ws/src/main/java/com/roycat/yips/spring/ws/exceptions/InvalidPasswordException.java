package com.roycat.yips.spring.ws.exceptions;

public class InvalidPasswordException extends Exception {

    private static final long serialVersionUID = 7603130804164894064L;

    public InvalidPasswordException() {
        super();
    }

    public InvalidPasswordException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public InvalidPasswordException(final String message) {
        super(message);
    }

    public InvalidPasswordException(final Throwable cause) {
        super(cause);
    }

}
