package com.roycat.yips.spring.ws.api;

import java.sql.SQLException;
import java.util.List;

import com.roycat.yips.spring.ws.exceptions.DuplicateEmailException;
import com.roycat.yips.spring.ws.exceptions.DuplicateUsernameException;
import com.roycat.yips.spring.ws.exceptions.InvalidPasswordException;
import com.roycat.yips.spring.ws.exceptions.MalformedEmailException;
import com.roycat.yips.spring.ws.models.User;

/**
 * This interface models all services related to User objects.
 * @author Steve Bolin - Apr 2018
 *
 */
public interface UserService {
    
    /**
     * Retrieves a list of all usernames
     * @return A list of all usernames
     * @throws SQLException
     */
    List<String> retrieveUsernames() throws SQLException;
    
    /**
     * Retrieves a list of all users
     * @return A list of all users. If no users are found, returns an empty list.
     * @throws SQLException
     */
    List<User> retrieveUsers() throws SQLException;
    
    /**
     * Retrieves the user with the associated id.
     * @param id The id of the user to retrieve.
     * @return The user with the associated id if such a user exists, otherwise returns null;
     * @throws SQLException
     */
    User retrieveUserById(Long id) throws SQLException;
    
    /**
     * Retrieves the user with the associated username.
     * @param username The username of the user to retrieve.
     * @return The user with the associated username if such a user exists, otherwise returns null.
     * @throws SQLException
     */
    User retrieveUserByUsername(String username) throws SQLException;
    
    /**
     * Searches the database for users where the username partially matches the 
     * username parameter, i.e. John would match johndoe and johnsmith. The matching
     * is case insensitive.
     * @param username The username or partial username to match to users.
     * @return A list of Users where username like username parameter. If no
     * matching users are found, returns an empty list.
     * @throws SQLException
     */
    List<User> searchUsersByUsername(String username) throws SQLException;
    
    /**
     * Searches the database for users where the email partially matches the 
     * email parameter The matching is case insensitive.
     * @param email The email or partial email to match to users.
     * @return A list of Users where email like email parameter. If no
     * matching users are found, returns an empty list.
     * @throws SQLException
     */
    List<User> searchUsersByEmail(String email) throws SQLException;

    /**
     * Registers the user with the application. 
     * @param user The user to be created.
     * @throws SQLException
     */
    void registerUser(User user) throws SQLException, DuplicateEmailException, MalformedEmailException, InvalidPasswordException, DuplicateUsernameException;

}
