package com.roycat.yips.spring.ws.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.roycat.yips.spring.ws.api.ChatController;
import com.roycat.yips.spring.ws.api.MessageService;
import com.roycat.yips.spring.ws.api.UserService;
import com.roycat.yips.spring.ws.models.Message;
import com.roycat.yips.spring.ws.models.User;

/**
 * For the sake of this demo, all methods requiring authorization are served in this controller.
 * @author Steve Bolin April 2018
 */
@RestController
@RequestMapping("/chat")
public class SimpleChatController implements ChatController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleChatController.class);

    private UserService userService;
    private MessageService messageService;

    @Autowired
    public SimpleChatController(final UserService userService, final MessageService messageService) {
        this.userService = userService;
        this.messageService = messageService;
    }

    public SimpleChatController() {
    }

    /* (non-Javadoc)
     * @see com.roycat.yips.spring.ws.controllers.ChatController#listUsernames()
     */
    @Override
    @RequestMapping(value = "/usernames", method = RequestMethod.GET)
    public final List<String> listUsernames() {

        List<String> users = null;

        try {
            users = this.userService.retrieveUsernames();
        } catch (SQLException e) {
            LOGGER.error("Failed to retrieve the list of all usernames", e);
        }

        return users;
    }

    /* (non-Javadoc)
     * @see com.roycat.yips.spring.ws.controllers.ChatController#listUsers()
     */
    @Override
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public final List<User> listUsers() {
        
        List<User> users = null;

        try {
            users = this.userService.retrieveUsers();
        } catch (SQLException e) {
            LOGGER.error("Failed to retrieve the list of all users", e);
        }

        return users;
    }

    /* (non-Javadoc)
     * @see com.roycat.yips.spring.ws.controllers.ChatController#searchUsersByUsername(java.lang.String)
     */
    @Override
    @RequestMapping(value = "/user/username/{username}", method = RequestMethod.GET)
    public final List<User> searchUsersByUsername(@PathVariable("username") final String username) {

        List<User> users = null;

        try {
            users = this.userService.searchUsersByUsername(username);
        } catch (SQLException e) {
            LOGGER.error("Failed to retrieve the user where the username = '" + username + "'.", e);
        }

        return users;
    }

    /* (non-Javadoc)
     * @see com.roycat.yips.spring.ws.controllers.ChatController#searchUsersById(java.lang.Long)
     */
    @Override
    @RequestMapping(value = "/user/id/{id:[\\d]+}", method = RequestMethod.GET)
    public final List<User> searchUsersById(@PathVariable("id") final Long id) {
        
        User user = null;

        try {
            user = this.userService.retrieveUserById(id);
        } catch (SQLException e) {
            LOGGER.error("Failed to retrieve the user where the id = '" + id + "'.", e);
        }

        return Arrays.asList(user);
    }

    /* (non-Javadoc)
     * @see com.roycat.yips.spring.ws.controllers.ChatController#searchUsersByEmail(java.lang.String)
     */
    @Override
    @RequestMapping(value = "/user/email/{email}", method = RequestMethod.GET)
    public final List<User> searchUsersByEmail(@PathVariable("email") final String email) {

        List<User> users = null;

        try {
            users = this.userService.searchUsersByEmail(email);
        } catch (SQLException e) {
            LOGGER.error("Failed to retrieve the user where the email = '" + email + "'.", e);
        }

        return users;
    }

    /* (non-Javadoc)
     * @see com.roycat.yips.spring.ws.controllers.ChatController#search(java.lang.Long, java.lang.String, java.lang.String)
     */
    @Override
    @RequestMapping(value = "/user/search", method = RequestMethod.GET)
    public final List<User> search(
        @RequestParam(value="id", required = false) final Long id, 
        @RequestParam(value="username", required = false) final String username,
        @RequestParam(value="email", required = false) final String email) {

        if (null != id) {
            return this.searchUsersById(id);
        } else if (null != username) {
            return this.searchUsersByUsername(username);
        } else if (null != email) {
            return this.searchUsersByEmail(email);
        } else {
            return null;
        }
    }


    /* (non-Javadoc)
     * @see com.roycat.yips.spring.ws.controllers.ChatController#getMessagesBetween(java.lang.Long, java.lang.Long)
     */
    @Override
    @RequestMapping(value = "/messages/between/{userId1:[\\d]+}/and/{userId2:[\\d]+}", method = RequestMethod.GET)
    public final List<Message> getMessagesBetween(@PathVariable("userId1") final Long userId1, @PathVariable("userId2") final Long userId2) {
        
        List<Message> messages = null;

        try {
            messages = this.messageService.retrieveMessagesBetween(userId1, userId2);
        } catch (SQLException e) {
            LOGGER.error("Failed to retrieve messages where userId1=" + userId1 + " and userId2 = " + userId2 + ".", e);
        }

        return messages;
    }

    /* (non-Javadoc)
     * @see com.roycat.yips.spring.ws.controllers.ChatController#postMessage(java.lang.String)
     */
    @Override
    @RequestMapping(value = "/message", method = RequestMethod.POST)
    public final ResponseEntity<?> postMessage(@RequestBody final String json) {
        ResponseEntity<?> result = null;
        
        ObjectMapper mapper = new ObjectMapper();
        Message message = null;
        try {
            message = mapper.readerFor(Message.class).readValue(json);
        } catch (IOException e1) {
            LOGGER.error("Failed to read message where json =" + json + ".", e1);
            result = ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
        }
        
        if (message != null) {
            try {
                this.messageService.createMessage(message);
                result = ResponseEntity.status(HttpStatus.ACCEPTED).build();
            } catch (SQLException e) {
                LOGGER.error("Failed to create message where message=" + message + ".", e);
                result = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
        }
        return result;        
    }

}
