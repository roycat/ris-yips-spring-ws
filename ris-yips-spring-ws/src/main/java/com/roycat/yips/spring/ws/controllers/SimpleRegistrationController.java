package com.roycat.yips.spring.ws.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.xml.ws.WebServiceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.roycat.yips.spring.ws.api.RegistrationController;
import com.roycat.yips.spring.ws.api.UserService;
import com.roycat.yips.spring.ws.exceptions.DuplicateEmailException;
import com.roycat.yips.spring.ws.exceptions.DuplicateUsernameException;
import com.roycat.yips.spring.ws.exceptions.InvalidPasswordException;
import com.roycat.yips.spring.ws.exceptions.MalformedEmailException;
import com.roycat.yips.spring.ws.models.User;

/**
 * @author Steve Bolin April 2018
 */
@RestController
@RequestMapping("/register")
public class SimpleRegistrationController implements RegistrationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleRegistrationController.class);

    private UserService userService;

    @Autowired
    public SimpleRegistrationController(final UserService userService) {
        this.userService = userService;
    }

    public SimpleRegistrationController() {
    }

    /* (non-Javadoc)
     * @see com.roycat.yips.spring.ws.controllers.RegistrationController#register(java.lang.String)
     */
    @Override
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public final ResponseEntity<?> register(@RequestBody final String json) throws WebServiceException {
        ResponseEntity<?> result = null;
        
        ObjectMapper mapper = new ObjectMapper();
        User user = null;
        try {
            user = mapper.readerFor(User.class).readValue(json);
        } catch (IOException e1) {
            LOGGER.error("Failed to read message where json =" + json + ".", e1);
            result = ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
        }
        
        if (user != null) {
            try {
                this.userService.registerUser(user);
                result = ResponseEntity.status(HttpStatus.ACCEPTED).build();
            } catch (SQLException e) {
                LOGGER.error("Failed to create user where user = " + user + ".", e);
                result = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            } catch (DuplicateEmailException e) {
                result = new ResponseEntity<String>(DuplicateEmailException.class.getSimpleName(), HttpStatus.UNPROCESSABLE_ENTITY);
            } catch (MalformedEmailException e) {
                result = new ResponseEntity<String>(MalformedEmailException.class.getSimpleName(), HttpStatus.UNPROCESSABLE_ENTITY);
            } catch (InvalidPasswordException e) {
                result = new ResponseEntity<String>(InvalidPasswordException.class.getSimpleName(), HttpStatus.UNPROCESSABLE_ENTITY);
            } catch (DuplicateUsernameException e) {
                result = new ResponseEntity<String>(DuplicateUsernameException.class.getSimpleName(), HttpStatus.UNPROCESSABLE_ENTITY);
            }
        }
        return result;
    }

}
