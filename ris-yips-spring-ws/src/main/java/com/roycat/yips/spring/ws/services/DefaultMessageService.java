package com.roycat.yips.spring.ws.services;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.roycat.yips.spring.ws.api.MessageDao;
import com.roycat.yips.spring.ws.api.MessageService;
import com.roycat.yips.spring.ws.models.Message;

/**
 * This class implements all services related to Message objects.
 * @author Steve Bolin - Apr 2018
 */
@Component
public class DefaultMessageService implements MessageService {

    private MessageDao messageDao;

    @Autowired
    public DefaultMessageService(final MessageDao messageDao) {
        this.messageDao = messageDao;
    }

    @Override
    public void createMessage(Message message) throws SQLException {
        this.messageDao.createMessage(message);
    }

    @Override
    public List<Message> retrieveMessagesBetween(Long userId1, Long userId2) throws SQLException {
        return this.messageDao.retrieveMessagesBetween(userId1, userId2);
    }

}
