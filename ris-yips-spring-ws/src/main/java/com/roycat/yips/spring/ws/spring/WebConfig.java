package com.roycat.yips.spring.ws.spring;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;;


@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {
                                "com.roycat.yips.spring.ws.controllers", 
                                "com.roycat.yips.spring.ws.daos", 
                                "com.roycat.yips.spring.ws.services", 
                                "com.roycat.yips.spring.ws.spring"
                               })
public class WebConfig implements WebMvcConfigurer {
   
    
    /**
     * Provides the DataSource from the JNDI lookup
     * @return The DataSource to use for connections to the db
     * @throws NamingException
     */
    @Bean(destroyMethod = "close")
    public DataSource dataSource() throws NamingException {
        DataSource result = null;
        try {
            Context initialContext = new InitialContext();
            Context context = (Context) initialContext.lookup("java:comp/env");
            result = (DataSource) context.lookup("jdbc/CHAT_WS");
        } catch (NamingException e) {
            throw new NamingException("Failed to look up the datasource details for CHAT_WS");
        }
        
        return result;
    }
}