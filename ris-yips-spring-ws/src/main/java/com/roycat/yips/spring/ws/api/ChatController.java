package com.roycat.yips.spring.ws.api;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.roycat.yips.spring.ws.models.Message;
import com.roycat.yips.spring.ws.models.User;

/**
 * This interface models the controller related to chat functionality. <br />
 * Controller mapping = "/chat"
 * @author Steve Bolin April 2018
 */
public interface ChatController {

    /**
     * Provide a List of the usernames of all users.<br />
     * Mapping = "/usernames", Method = GET
     * @return a List of the usernames of all users.
     */
    List<String> listUsernames();

    /**
     * Provide a List of all User objects with the passwords masked.<br />
     * Mapping = "/users", Method = GET
     * @return a List of all User objects with the passwords masked.
     */
    List<User> listUsers();

    /**
     * Provide a List of all users where the users username contains 
     * the username parameter.<br />
     * Mapping = "/user/username/{username}", Method = GET
     * @param username The username to compare
     * @return a List of all users where the users username contains 
     * the username parameter.
     */
    List<User> searchUsersByUsername(String username);

    /**
     * Provide a List of all users where the user id matches the id parameter. 
     * This list will only contain one element.<br />
     * Mapping = "/user/id/{id:[\\d]+}", Method = GET
     * @param id The id to find.
     * @return a List of all users where the user id matches the id parameter. 
     * This list will only contain one element.
     */
    List<User> searchUsersById(Long id);

    /**
     * Provide a List of all users where the users email contains 
     * the email parameter.<br />
     * Mapping = "/user/email/{email}", Method = GET
     * @param email The email to compare
     * @return a List of all users where the users email contains 
     * the email parameter.
     */
    List<User> searchUsersByEmail(String email);

    /**
     * Provide a List of users where the provided parameters equal, or contain the
     * associated fields of the listed users. Only one of the parameters is required
     * at a time and the other two may be null. <br />
     * Mapping = "/user/search", Method = GET<br />
     * Request parameters are: id, username, and email. See below for examples.<br />
     * <b>Example usage: <br />
     * /user/search?id=1 <br />
     * /user/search?username=john <br />
     * /user/search?email=yips <br />
     * /user/search?email=@gmail.com </b><br />
     * 
     * @param id The id to match. This performs an equals match.
     * @param username The username to match. This performs a contains match.
     * @param email The email to match. This performs a contains match.
     * @return a List of users where the provided parameters equal, or contain the
     * associated fields of the listed users.
     */
    List<User> search(Long id, String username, String email);

    /**
     * Provides a List of all messages between the two users as identified by the user id. <br />
     * Mapping = "/messages/between/{userId1:[\\d]+}/and/{userId2:[\\d]+}", Method = GET
     * @param userId1 The user id of one of the users.
     * @param userId2 The user id of a different user.
     * @return a List of all messages between the userId1 and userId2.
     */
    List<Message> getMessagesBetween(Long userId1, Long userId2);

    /**
     * Creates a new message in the system.
     * <br />
     * <b>JSON Format of Message object to submit.</b><br />
     * <code>
     * { <br />
     *  "fromUserId": 1,<br />
     *  "toUserId": 2,<br />
     *  "text": "Message text goes here.",<br />
     *  "createdTime": "yyyy-MM-dd HH:mm:ss"<br />
     *  }
     * </code><br />
     * RequestMapping(value = "/message", method = RequestMethod.POST)
     * @param json
     * @return A response  indicating ACCEPTED, or UNPROCESSABLE_ENTITY. If UNPROCESSABLE_ENTITY, the Exception name will 
     * be returned as content.
     */
    ResponseEntity<?> postMessage(String json);

}
