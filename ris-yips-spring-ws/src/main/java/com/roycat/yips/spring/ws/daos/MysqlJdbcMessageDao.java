package com.roycat.yips.spring.ws.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.roycat.yips.spring.ws.api.MessageDao;
import com.roycat.yips.spring.ws.models.Message;

@Component
public class MysqlJdbcMessageDao extends AbstractJdbcDao implements MessageDao {
        
    private DataSource dataSource;
    
    @Autowired
    public MysqlJdbcMessageDao(final DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void createMessage(Message message) throws SQLException {

        String sql = "insert into message (from_user_id, to_user_id, message_text, create_time) values (?, ?, ?, ?)";

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = this.dataSource.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setLong(1, message.getFromUserId());
            ps.setLong(2, message.getToUserId());
            ps.setString(3, message.getText());
            ps.setTimestamp(4, Timestamp.valueOf(message.getCreatedTime()));
            ps.execute();

        } catch (SQLException e) {
            throw new SQLException("Failed to create message where message = " + message + ".", e);
        } finally {
            super.close(conn, ps, rs);
        }        
    }

    @Override
    public List<Message> retrieveMessagesBetween(Long userId1, Long userId2) throws SQLException {
        List<Message> result = new ArrayList<>();
        
        String sql = "select id, from_user_id, to_user_id, message_text, create_time from message "
            + "where from_user_id=? and to_user_id=? "
            + "or from_user_id=? and to_user_id=? "
            + "order by create_time ASC";

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = this.dataSource.getConnection();

            ps = conn.prepareStatement(sql);
            ps.setLong(1, userId1);
            ps.setLong(2, userId2);
            ps.setLong(3, userId2);
            ps.setLong(4, userId1);
            rs = ps.executeQuery();
            while (rs.next()) {
                result.add(this.populateMessageModel(rs));
            }

        } catch (SQLException e) {
            throw new SQLException("Failed to retrieve user messages where userId1 = " + userId1 + " and userId2= " + userId2 + ".", e);
        } finally {
            super.close(conn, ps, rs);
        }
        return result;
    }

    private Message populateMessageModel(ResultSet rs) throws SQLException {
        Message result = new Message();
        result.setId(rs.getLong("id"));
        result.setText(rs.getString("message_text"));
        result.setToUserId(rs.getLong("to_user_id"));
        result.setFromUserId(rs.getLong("from_user_id"));
        result.setCreatedTime(rs.getTimestamp("create_time").toLocalDateTime());
        return result;
    }

}
