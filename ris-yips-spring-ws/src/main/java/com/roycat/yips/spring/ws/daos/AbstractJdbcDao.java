package com.roycat.yips.spring.ws.daos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractJdbcDao {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractJdbcDao.class);

	public void close(final Connection conn, final Statement stmt, final ResultSet rs) {
		closeQuietly(stmt);
		closeQuietly(rs);
		closeQuietly(conn);
	}

	public static void closeQuietly(Connection connection) {
		try {
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			LOGGER.error("An error occurred closing connection.", e);
		}
	}

	public static void closeQuietly(Statement statement) {
		try {
			if (statement != null) {
				statement.close();
			}
		} catch (SQLException e) {
			LOGGER.error("An error occurred closing statement.", e);
		}
	}

	public static void closeQuietly(ResultSet resultSet) {
		try {
			if (resultSet != null) {
				resultSet.close();
			}
		} catch (SQLException e) {
			LOGGER.error("An error occurred closing result set.", e);
		}
	}

}
