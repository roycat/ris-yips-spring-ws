package com.roycat.yips.spring.ws.exceptions;

public class DuplicateUsernameException extends Exception {

    private static final long serialVersionUID = 7603130804164894063L;

    public DuplicateUsernameException() {
        super();
    }

    public DuplicateUsernameException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public DuplicateUsernameException(final String message) {
        super(message);
    }

    public DuplicateUsernameException(final Throwable cause) {
        super(cause);
    }

}
