package com.roycat.yips.spring.ws.spring;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.roycat.yips.spring.ws.api.UserService;
import com.roycat.yips.spring.ws.constants.RoleConstants;
import com.roycat.yips.spring.ws.models.BasicUserDetails;
import com.roycat.yips.spring.ws.models.User;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomAuthenticationProvider.class);
    
    private UserService chatService;
    
    @Autowired
    public CustomAuthenticationProvider(final UserService chatService) {
        this.chatService = chatService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Authentication result = null;
        
        String name = authentication.getName();
        String password = null != authentication.getCredentials() ? authentication.getCredentials().toString() : "";

        
        UserDetails principal = this.findUser(name);
        if (null != principal) {
            boolean passwordsMatch = principal.getPassword().equals(password);
            
            if (passwordsMatch) {
                final List<GrantedAuthority> grantedAuths = new ArrayList<>();
                grantedAuths.add(new SimpleGrantedAuthority(RoleConstants.USER));
                
                result = new UsernamePasswordAuthenticationToken(principal, password, grantedAuths);
            }
        }
        return result;        
    }
    
    @Override
    public boolean supports(Class<?> auth) {
        boolean result =  (UsernamePasswordAuthenticationToken.class.isAssignableFrom(auth));
        
        return result;
    }
    
    private UserDetails findUser(String username) throws UsernameNotFoundException {
        User user = null;
        UserDetails result = null;
        try {
            user = this.chatService.retrieveUserByUsername(username);
            if (null != user) {
                result = new BasicUserDetails(user);
            }
        } catch (SQLException e) {
            LOGGER.error("Failed to retrieve user by username where username = [" + username + "].", e);
            throw new UsernameNotFoundException(
                "There was a SQLException which prevented the username from being found.", e);
        }

        return result;
    }

}
