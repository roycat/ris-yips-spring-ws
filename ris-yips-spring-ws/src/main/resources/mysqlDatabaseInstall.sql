-- Below is the sql to create the tables and sample data for the demo using mysql

CREATE TABLE app_user (
	id MEDIUMINT NOT NULL AUTO_INCREMENT,
	pword VARCHAR(64) NOT NULL,
	email varchar(128) NOT NULL,
	username varchar(128) NOT NULL,
	PRIMARY KEY(id),
	UNIQUE KEY(email),
	UNIQUE KEY(username)
);

insert into app_user (pword, email, username) values 
("password1", "test1@yiptest.com", "Yips1"), 
("password2", "test2@yiptest.com", "Yips2");

CREATE TABLE message (
	id MEDIUMINT NOT NULL AUTO_INCREMENT,
	from_user_id MEDIUMINT NOT NULL,
	to_user_id MEDIUMINT NOT NULL,
	message_text varchar(512),
	create_time DATETIME NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(from_user_id) REFERENCES app_user(id),
	FOREIGN KEY(to_user_id) REFERENCES app_user(id)
);

insert into message (from_user_id, to_user_id, message_text, create_time) values
(1, 2, 'Hey there.', '2018-04-26 15:35:01'),
(2, 1, 'Hey there Back to you.', '2018-04-26 15:37:01');